import { Disclosure } from "@headlessui/react";
import { ChevronUpIcon } from "@heroicons/react/solid";

export const Accordion = () => {
  return (
    <>
      <div className="mt-2 flex gap-3 rounded-md shadow-sm">
        <span className="inline-flex items-center px-3 rounded-l-md border  border-r-0 sm:text-sm border-gray-300 bg-gray-50 text-gray-500 text-sm">
          Album
        </span>
        <label className="block text-sm font-medium text-gray-700">
          Utopia
        </label>
      </div>
      <div className="mt-2 gap-3 flex rounded-md shadow-sm">
        <span className="inline-flex items-center px-3 rounded-l-md border  sm:text-sm border-r-0 border-gray-300 bg-gray-50 text-gray-500 text-sm">
          Artista
        </span>
        <label className="block text-sm font-medium text-gray-700">
          Travis Sccot
        </label>
      </div>
      <Disclosure as="div" className="mt-2">
        {({ open }) => (
          <>
            <Disclosure.Button className=" transition-all flex w-full justify-between rounded-lg bg-[#ff7d47] hover:bg-orange-500 focus:outline-none focus:ring-2 px-4 py-2 text-left text-sm font-medium text-white  focus-visible:ring focus-visible:ring-purple-500 focus-visible:ring-opacity-75">
              <span>Melhores Musicas</span>
              <ChevronUpIcon
                className={`${
                  open ? "rotate-180 transform" : ""
                } h-5 w-5 text-white`}
              />
            </Disclosure.Button>
            <Disclosure.Panel className="px-4 pt-4 pb-2 text-sm text-gray-500">
              No.
            </Disclosure.Panel>
          </>
        )}
      </Disclosure>
    </>
  );
};
