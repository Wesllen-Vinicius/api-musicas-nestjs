import React from "react";
import axios from "axios";

export class ArtistaList extends React.Component {
  state = {
    artistas: [],
  };

  componentDidMount() {
    axios.get(`http://localhost:3001/artistas`).then((res) => {
      const artistas = res.data;
      this.setState({ artistas });
    });
  }

  render() {
    return (
      <select
        className="form-select appearance-none
      w-full
      px-3
      py-1.5
      text-gray-700
      bg-white bg-clip-padding bg-no-repeat
      border border-solid border-gray-300
      rounded
      transition
      mt-2

      focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
        aria-label="Default select example"
      >
        <option selected>Selecione um Artista</option>
        {this.state.artistas.map((artista) => (
          <option>{artista.f_name} {artista.l_name}</option>
        ))}
      </select>
    );
  }
}
