import axios from "axios";
import { useEffect, useState } from "react";
import { useForm, SubmitHandler } from "react-hook-form";
import { api } from "../services/axios";
import { Toast } from "../utils/toast";
interface Artista {
  id: number;
  f_name: string;
  l_name: string;
}
interface IFormValues {
  nome_album: string;
  autor: string;
  artistaId: number;
}

export default function CadastroAlbum() {
  const { register, handleSubmit } = useForm<IFormValues>();
  const [artista, setArtista] = useState<Artista[]>([]);

  const onSubmit: SubmitHandler<IFormValues> = async (data) => {
    try {
      await api.post("albuns", {
        titulo: data.nome_album,
        artistaId: Number(data.artistaId),
      });
      Toast.fire({
        icon: "success",
        title: "Cadastro realizado com sucesso!",
      });
    } catch (e: any) {
      Toast.fire({
        icon: "error",
        title: e.message,
      });
    }
  };

  useEffect(() => {
    api.get(`artistas`).then((res) => {
      const artista = res.data;
      setArtista(artista);
    });
  }, []);

  return (
    <>
      <div className="md:grid  md:gap-6 place-items-center">
        <div className="mt-5 md:mt-0 max-w-xl w-full">
          <form onSubmit={handleSubmit(onSubmit)} method="POST">
            <div className="shadow-xl sm:rounded-md sm:overflow-hidden bg-white">
              <div className="px-4 py-5  space-y-6 sm:p-6 ">
                <div className="grid grid-cols-3 gap-6 ">
                  <div className="col-span-3 sm:col-span-2">
                    <label
                      htmlFor="company-website"
                      className="block text-sm font-medium text-gray-700"
                    >
                      Album
                    </label>
                    <div className="mt-2 flex rounded-md shadow-sm">
                      <span className="w-[80px]  inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-300 bg-gray-50 text-gray-500 text-sm">
                        Titulo
                      </span>
                      <input
                        {...register("nome_album")}
                        type="text"
                        required
                        className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300  placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                        placeholder="titulo"
                      />
                    </div>
                    <div className="mt-2 flex rounded-md shadow-sm">
                      <span className="w-[80px]  inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-300 bg-gray-50 text-gray-500 text-sm">
                        Artista
                      </span>
                      <select
                        {...register("artistaId")}
                        className="form-select appearance-none
                        w-full
                        px-3
                        py-1.5
                        text-gray-700
                        bg-white bg-clip-padding bg-no-repeat
                        border border-solid border-gray-300
                        rounded
                        transition
                        mt-2
                      focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                        aria-label="Default select example"
                      >
                        <option selected>Selecione um Artista</option>
                        {artista.map((artista) => (
                          <option key={artista.id} value={artista.id}>
                            {artista.f_name} {artista.l_name}
                          </option>
                        ))}
                      </select>
                    </div>
                    <div className="mt-2 flex rounded-md shadow-sm">
                      <span className="w-[80px]  inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-300 bg-gray-50 text-gray-500 text-sm">
                        Ano
                      </span>
                      <input
                        id="ano"
                        name="ano"
                        type="date"
                        required
                        className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300  placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                      />
                    </div>
                  </div>
                </div>
                <div>
                  <label className="block text-sm font-medium text-gray-700">
                    Foto de Capa
                  </label>
                  <div className="mt-1 flex justify-center px-6 pt-5 pb-6 border-2 border-gray-300 border-dashed rounded-md">
                    <div className="space-y-1 text-center">
                      <svg
                        className="mx-auto h-12 w-12 text-gray-400"
                        stroke="currentColor"
                        fill="none"
                        viewBox="0 0 48 48"
                        aria-hidden="true"
                      >
                        <path
                          d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02"
                          strokeWidth={2}
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        />
                      </svg>
                      <div className="flex text-sm text-gray-600">
                        <label
                          htmlFor="file-upload"
                          className="relative cursor-pointer bg-white rounded-md font-medium text-indigo-600 hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500"
                        >
                          <span>Upload</span>
                          <input
                            id="file-upload"
                            name="file-upload"
                            type="file"
                            className="sr-only"
                          />
                        </label>
                        <p className="pl-1">Selecione um Arquivo</p>
                      </div>
                      <p className="text-xs text-gray-500">PNG, JPG, 10MB</p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
                <button type="submit" className="button">
                  Save
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div className="hidden sm:block" aria-hidden="true">
        <div className="py-5">
          <div className="border-t border-gray-700"></div>
        </div>
      </div>
    </>
  );
}
