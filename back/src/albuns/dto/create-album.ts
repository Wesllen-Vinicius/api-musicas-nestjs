import { IsNotEmpty, IsString } from 'class-validator';

export class CreateAlbum {
  @IsString()
  @IsNotEmpty()
  nome_album: string;
}
